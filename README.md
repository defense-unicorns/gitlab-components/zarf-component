# Zarf (setup, package create, package publish) components

## Zarf setup

Setup the zarf cli

### Usage

Use this component to install the zarf cli in a pipeline.

You should add this component to an existing `.gitlab-ci.yml` file by using the `include:`
keyword.

```yaml
include:
  - component: ${CI_SERVER_HOST}/${GITLAB_COMPONENT_GROUP}/zarf-component/zarf-setup@<VERSION>
```

where `<VERSION>` is the latest released tag or `main`.

where `${GITLAB_COMPONENT_GROUP}` is a CICD variable with the path to the group containing the gitlab component definitions.

This adds a job called `zarf-setup` to the pipeline.

#### Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | `build`      | The stage where you want the job to be added |
| `zarf_image` | `curlimages/curl` | The Docker image for the job  |
| `version` | `"latest"` | The version of the zarf cli  |

## Zarf package create

Create a zarf package

### Usage

Use this component to build a zarf package.

You should add this component to an existing `.gitlab-ci.yml` file by using the `include:`
keyword.

```yaml
include:
  - component: ${CI_SERVER_HOST}/${GITLAB_COMPONENT_GROUP}/zarf-component/zarf-package-create@<VERSION>
```

where `<VERSION>` is the latest released tag or `main`.

where `${GITLAB_COMPONENT_GROUP}` is a CICD variable with the path to the group containing the gitlab component definitions.

This adds a job called `zarf-package-create` to the pipeline.

#### Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `path` | `.`      | The path to the directory where your zarf.yaml is located (Defaults to current working directory) |
| `stage` | `build`      | The stage where you want the job to be added |
| `zarf_image` | `ghcr.io/defenseunicorns/swf-pipeline-templates/zarf` | The Docker image for the job  |
| `zarf_version` | `v0.31.3` | The Docker image tag for the job  |
| `pkg_image` | `"${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}"` | The Docker image for the zarf package  |
| `pkg_version` | `"${CI_COMMIT_SHORT_SHA}"` | The version for the zarf package  |

## Zarf package publish

Publish a zarf package

### Usage

Use this component to publish a zarf package to an oci compatible registry.

You should add this component to an existing `.gitlab-ci.yml` file by using the `include:`
keyword.

```yaml
include:
  - component: ${CI_SERVER_HOST}/${GITLAB_COMPONENT_GROUP}/zarf-component/zarf-package-publish@<VERSION>
```

where `<VERSION>` is the latest released tag or `main`.

where `${GITLAB_COMPONENT_GROUP}` is a CICD variable with the path to the group containing the gitlab component definitions.

This adds a job called `zarf-package-publish` to the pipeline.

#### Inputs

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `path` | `*.tar.zst`      | The path to the directory where your zarf package is located (Defaults to a package in the current working directory) |
| `stage` | `build`      | The stage where you want the job to be added |
| `zarf_image` | `ghcr.io/defenseunicorns/swf-pipeline-templates/zarf` | The Docker image for the job  |
| `zarf_version` | `v0.31.3` | The Docker image tag for the job  |
| `destination_registry` | `"oci://${CI_REGISTRY_IMAGE}"` | The oci compliant registry destination for publishing the package  |


## Contribute

Please read about CI/CD components and best practices at: https://docs.gitlab.com/ee/ci/components 
